#!/usr/local/bin/perl
use warnings;
use 5.016;
use Getopt::Long qw(:config no_ignore_case);

GetOptions(
  'f=s'   => \my $field,
  'd=s'   => \my $delimiter,
  's'     => \my $separated,
);

if((!defined $field) and (!defined $delimiter) and (!defined $separated)) {
  print STDERR 'cut: you must specify a list of fields';
  exit(1);
}

if ((!defined $field) or (!defined $delimiter) or (!defined $separated)) {
  $field //= '';
  $delimiter //= "\t";
}
if (length $delimiter != 1 and length $delimiter != 0) {
  print STDERR 'cut: the delimiter must be a single character';
  exit(1);
}
if ($field eq '0') {
  print STDERR 'cut: fields are numbered from 1';
  exit(1);
}
if ($field =~ /[a-z]/gi) {
  printf STDERR "cut: invalid field value '%s'", $field;
  exit(1);
}

my (@columnss) = split(',', $field);

for my $k (0..$#columnss) {
  if ( index '-', $columnss[$k]) {

    my @result = split('-', $columnss[$k]);

    for my $i(1..$#result) {
      if ($result[$i-1] gt $result[$i]) {
        print STDERR 'cut: invalid decreasing range';
        exit(1);
      }
    }
  }
  if (($columnss[$k] + 0) > 1000000) {
    printf STDERR "cut: field number '%s' is too large", $field;
    exit(1);
  }
}

my $stroke;
my $link;
my $row = 0;
my @columns;

while(defined($link = <>)) {

  chomp($link);
  @columns = split ($delimiter, $link);

  if (defined $separated and (@columns == 1)) {
    next;
  }

  my (@fields) = uniq(sort(split(',', $field)));

  for my $k (0..$#fields) {
    if((index $fields[$k], '-') >= 0) {
      my $begin = index $fields[$k], '-';
      if ($begin == 0) {
        my $column = ((substr $fields[$k], 1) + 0);
        for my $t (0..($column-1)) {

          if (defined($columns[$t])) {
            print "$columns[$t]";

            if (@columns == 1) {
              next;
            }

            if(($t < ($column-1)) and ($k <= $#fields)) {
              print "$delimiter";
            }

            elsif(defined($fields[$k+1]) and ($fields[$k+1]-1 <= $#columns+1) and ($t == ($#columns-1))) {
              print "$delimiter";
            }
          }
        }
      }
      elsif(($begin == 1) and (length $fields[$k] == 3)) {
        for my $t (((substr $fields[$k], 0)-1)..((substr $fields[$k], 2)-1)) {

          if (defined($columns[$t])) {
            $columns[$t] =~ s/\s+$//;
            print "$columns[$t]";
            if (@columns == 1) {
              next;
            }
            if(($t < (substr $fields[$k], 2) - 1) and ($k <= $#fields)) {
              print "$delimiter";
            }
          }
        }
      }
      elsif(($begin == 1) and (length $fields[$k] == 2)) {
        for my $t (((substr $fields[$k], 0) - 1)..$#columns) {

          if (defined($columns[$t])) {
            $columns[$t] =~ s/\s+$//;
            print "$columns[$t]";
            if (@columns == 1) {
              next;
            }
            if(($t < $#columns) and ($k <= $#fields)) {
              print "$delimiter";
            }
            if(defined($fields[$k+1]) and ($t < ($#columns-1))) {
              print "$delimiter";
            }
          }
        }
      }
    }
    else {
      $row  = $row + 1;
      if (defined($columns[$fields[$k]-1])) {
        $columns[$fields[$k]-1] =~ s/\s+$//;
        if ($k != 0  and $row <= 1) {
          print "$delimiter";
        }
        print "$columns[$fields[$k]-1]";
        if (@columns == 1) {
          next;
        }
        if($k < $#fields) {
          print "$delimiter";
        }
      }
    }
  }
  $row = 0;
  print "\n";
}

sub uniq {
    my %seen;
    grep !$seen{$_}++, @_;
}
