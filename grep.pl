#!/usr/bin/env perl

use strict;
use warnings;
# use Getopt::Long; # для обработки аргументов
use 5.016;
use Getopt::Long qw(:config no_ignore_case);

my @links;
my $i = 0;
my $number;

GetOptions(
    'A=i'    => \my $after,        #numeric
    'B=i'    => \my $before,       #numric
    'C=i'    => \my $complex,      #numeric
    'c'      => \my $count,        #flag
    'v'      => \my $invert,       #flag
    'F'      => \my $fixed,        #flag
    'n'      => \my $number_line,  #flag
    'i'      => \my $ignore_case,  #flag
    'color!' => \my $color,        #flag
);

my $regular_expr = shift@ARGV;

while (defined($number = <>)) {
    chomp($number);
    $links[$i] = $number;
    $i++;
 }

my $match_srt = 0;
$after//= 0;
$before//= 0;
$complex//= 0;

if ($complex > $before) {
    $before = $complex;
}

if ($complex > $after) {
    $after = $complex;
}

if ($invert) {
    $after = 0;
    $before = 0;
}

for my $i (0..$#links) {
    if (!defined $fixed && $links[$i] =~ /$regular_expr/){
L2:     if (!defined $ignore_case && !defined $invert) {
            if ($count) {
                $match_srt = $match_srt + 1;
                next;
            }
L1:
            flag_B($i, $before);
            printing($i, $links[$i]);
            flag_A($i, $after);
    }

        elsif (!defined $ignore_case && $invert) {
            if ($count) {
                next;
            }
            flag_B($i, $before);
            flag_A($i, $after);
            if ($complex) {
                printing($i, $links[$i]);
            }
        }
    }

    elsif(!defined $fixed && !($links[$i] =~ /$regular_expr/)) {
L3:     if (!defined $ignore_case && $invert) {
            if ($count) {
                $match_srt = $match_srt + 1;
                next;
            }
            printing($i, $links[$i]);
        }
    }

    elsif(!defined $fixed && ($links[$i] =~ /$regular_expr/i) && $ignore_case) {
L4:     if (!defined $invert) {
            if ($count) {
                $match_srt = $match_srt + 1;
                next;
            }
            goto L1;
        }

        else {
            if ($count) {
                $match_srt = $match_srt + 1;
                next;
            }
            flag_B($i, $before);
            flag_A($i, $after);
        }
    }

    elsif(!defined $fixed && !($links[$i] =~ /$regular_expr/i) && $ignore_case) {
L5:     if ($invert) {
            if ($count) {
                next;
            }
            printing($i, $links[$i]);
        }
    }

    elsif($fixed && $links[$i] =~ /\Q$regular_expr\E/) {
        goto L2;
    }

    elsif($fixed && !($links[$i] =~ /\Q$regular_expr\E/)) {
        goto L3;
    }

    elsif($fixed && ($links[$i] =~ /\Q$regular_expr\E/i) && $ignore_case) {
        goto L4;
    }

    elsif($fixed && !($links[$i] =~ /\Q$regular_expr\E/i) && $ignore_case) {
        goto L5;
    }
}

if ($count) {
    print $match_srt;
}

sub flag_A {
    my ($stroke) = $_[0];
    my ($down) = $_[1];

    if ($down == 0) {
        return;
    }

    for my $i ($stroke..($stroke + $down - 1)) {
        if ($i + 1 > $#links) {
            next;
        }
        printing($i+1, $links[$i+1]);
    }
}

sub flag_B {
    my ($stroke) = $_[0];
    my ($up) = $_[1];

    if ($up == 0) {
        return;
    }

    my $h;

    for my $i (0..($up-1)) {
        $h = $stroke - $up + $i;
        if ($h < 0) {
            next;
        }
        printing($h, $links[$h]);
    }
}
sub printing {
    my ($number) = $_[0];
    my ($stroke) = $_[1];

    if ($number_line) {
        if (($number+1) % 2) {
            printf "%d-%s\n", $number+1, $stroke;
        }
        else {
            printf "%d:%s\n", $number+1, $stroke;
        }
    }

    else {
        print "$stroke\n";
    }
}
